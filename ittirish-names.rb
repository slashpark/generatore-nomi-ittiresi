#!/usr/bin/ruby

# names remote path
$names_url = 'https://gitlab.com/slashpark/generatore-nomi-ittiresi/raw/master/names.txt'
$config_folder = '~/.ittirish-names'
$config_folder = File.expand_path($config_folder) # choose the right home directory
$names_file = $config_folder + '/names.txt'

def downloadNames()
  # Download names file
  puts '', 'UPDATING NAMES LIST', ''
  system('rm -rf ' + $names_file)
  system('wget ' + $names_url + ' -P ' + $config_folder)
end

def main()
  names = []
  # Check if config folder exist
  if File.exist?($config_folder)
  # Check if names file exist
    if !File.exist?($names_file)
      downloadNames()
    end
  else
    downloadNames()
  end

  # Load names into array
  File.open($names_file, 'r') do |f|
    f.each_line do |line|
      names.append(line)
    end
  end

  r = Random.new
  puts 'Your Ittirish name is: ' + names[r.rand(0...names.length)]
end

def checkARGV()
  if ARGV[0] == nil
    main()
  elsif ARGV[0] == '-u'
    downloadNames()
  elsif ARGV[0] == '-h'
    puts '-u      Update names list', '-h      Print this screen', 'Launch without arguments to print a random name'
  end
end

checkARGV()